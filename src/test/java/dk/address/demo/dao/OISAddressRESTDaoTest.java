/**
 * 
 */
package dk.address.demo.dao;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.OISAddress;
import dk.address.demo.utility.Response2ReturnType;

/**
 * @author Tau
 *
 */
public class OISAddressRESTDaoTest {

	/**
	 * Test method for {@link dk.address.demo.dao.KVHAddressRESTDao#getData(dk.address.demo.utility.Response2ReturnType, java.lang.Object[])}.
	 * @throws ParserReturnTypeException 
	 * @throws ClientServiceException 
	 */
	@Test
	public void testIntegrationGetData() throws ClientServiceException, ParserReturnTypeException {
		OISAddressRESTDao<String> service = new OISAddressRESTDao<>();
				
		List<String> respons = service.getData(new Response2ReturnType<String>() {
			
			@Override
			public String parse(Object item) {
				OISAddress address = (OISAddress) item;
				
				assertEquals("110", address.getHusnr());
				assertEquals("Skovparken", address.getVejnavn().getNavn());
				assertEquals("4600", address.getPostnummer().getNr());
				
				return address.getHusnr();
			}
		}, "Skovparken 110, 4600");
		
		assertNotNull(respons);
		assertEquals(1, respons.size());
		assertEquals("110",respons.get(0));
	}

}
