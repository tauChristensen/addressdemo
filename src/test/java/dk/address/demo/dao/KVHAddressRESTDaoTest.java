/**
 * 
 */
package dk.address.demo.dao;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.KVHAddress;
import dk.address.demo.utility.Response2ReturnType;

/**
 * @author Tau
 *
 */
public class KVHAddressRESTDaoTest {

	/**
	 * Test method for {@link dk.address.demo.dao.KVHAddressDao#getData(dk.address.demo.utility.Response2ReturnType, java.lang.Object[])}.
	 * @throws ParserReturnTypeException 
	 * @throws ClientServiceException 
	 */
	@Test
	public void testIntegrationGetData() throws ClientServiceException, ParserReturnTypeException {
		KVHAddressRESTDao<String> service = new KVHAddressRESTDao<>();
				
		List<String> response = service.getData(new Response2ReturnType<String>() {
			
			@Override
			public String parse(Object item) {
				assert item instanceof KVHAddress;
				
				KVHAddress address = (KVHAddress) item;
				
				assertEquals("110", address.getHusnr());
				assertEquals("Skovparken", address.getVejnavn());
				assertEquals("4600", address.getPostnummer());
				
				return address.getHusnr();
			}
		}, "Skovparken 110, 4600");
		
		assertNotNull(response);
		assertEquals(1, response.size());
		assertEquals("110",response.get(0));
	}
	
	@Test
	public void testIntegrationCharsetGetData() throws ClientServiceException, ParserReturnTypeException, UnsupportedEncodingException {
		KVHAddressRESTDao<String> service = new KVHAddressRESTDao<>();
				
		List<String> response = service.getData(new Response2ReturnType<String>() {
			
			@Override
			public String parse(Object item) {
				assert item instanceof KVHAddress;
				
				KVHAddress address = (KVHAddress) item;
				
				assertEquals("N�rregade", address.getVejnavn());
				assertEquals("K�benhavn K", address.getPostnavn());
				
				return address.getHusnr();
			}
		}, "N�rregade 24, K�benhavn K");
		
		assertNotNull(response);
		assert 1 <= response.size();	
	}
}
