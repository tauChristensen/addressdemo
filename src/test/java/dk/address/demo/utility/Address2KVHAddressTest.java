/**
 * 
 */
package dk.address.demo.utility;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.KVHAddress;
import dk.address.demo.info.OISAddress;
import dk.address.demo.info.OISAddressKommune;
import dk.address.demo.info.OISAddressPostnummer;
import dk.address.demo.info.OISAddressVejnavn;
import dk.address.demo.utility.Address2KVHAddress;

/**
 * Test parse function from Address2KVHAddress
 * 
 * @author Tau
 *
 */
public class Address2KVHAddressTest {

	Address2KVHAddress parser = new Address2KVHAddress();
	
	@Test
	public void testNull() throws ParserReturnTypeException {
		KVHAddress item = parser.parse(null);
		
		assertNull("KVHAddress not null", item);
	}
	
	@Test
	public void testEqual() throws ParserReturnTypeException {
		OISAddress address = new OISAddress();
		address.setHusnr("17");
		address.setKommune(new OISAddressKommune());
		address.getKommune().setKode("101");
		address.getKommune().setNavn("KÝbenhavn");
		address.setPostnummer(new OISAddressPostnummer());
		address.getPostnummer().setNr("1000");
		address.getPostnummer().setNavn("KÝbenhavn PostBox");
		address.setVejnavn(new OISAddressVejnavn());
		address.getVejnavn().setKode("2100");
		address.getVejnavn().setNavn("Nygaden");
		
		KVHAddress item = parser.parse(address);
		
		assertNotNull(item);
		
		assertEquals(address.getHusnr(), item.getHusnr());
		assertEquals(address.getKommune().getKode(), item.getKommune());
		assertEquals(address.getVejnavn().getNavn(), item.getVejnavn());
		assertEquals(address.getVejnavn().getKode(), item.getVejnummer());
		assertEquals(address.getPostnummer().getNavn(), item.getPostnavn());
		assertEquals(address.getPostnummer().getNr(), item.getPostnummer());
	}
}
