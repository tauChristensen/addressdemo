package dk.address.demo;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dk.address.demo.dao.RESTClientServiceDaoTest;
import dk.address.demo.dao.RESTClientServiceDao;
import dk.address.demo.utility.Address2KVHAddressTest;


@RunWith(Suite.class)
@SuiteClasses({Address2KVHAddressTest.class, RESTClientServiceDaoTest.class})
public class UnitTests {

}
