package dk.address.demo;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({dk.address.demo.dao.KVHAddressRESTDaoTest.class, 
			   dk.address.demo.dao.OISAddressRESTDaoTest.class})
public class IntegrationTests {

}
