package dk.address.demo.info;

/**
 * Return data from AddressDemo application
 * 
 * @author Tau
 *
 */

public class KVHAddress {
	
	private String kommune;
	private String vejnummer;
	private String postnummer;
	private String postnavn;
	private String vejnavn;
	private String husnr;
	
	public String getKommune() {
		return kommune;
	}
	public void setKommune(String kommune) {
		this.kommune = kommune;
	}
	public String getVejnummer() {
		return vejnummer;
	}
	public void setVejnummer(String vejnummer) {
		this.vejnummer = vejnummer;
	}
	public String getPostnummer() {
		return postnummer;
	}
	public void setPostnummer(String postnummer) {
		this.postnummer = postnummer;
	}
	public String getPostnavn() {
		return postnavn;
	}
	public void setPostnavn(String postnavn) {
		this.postnavn = postnavn;
	}
	public String getVejnavn() {
		return vejnavn;
	}
	public void setVejnavn(String vejnavn) {
		this.vejnavn = vejnavn;
	}
	public String getHusnr() {
		return husnr;
	}
	public void setHusnr(String husnr) {
		this.husnr = husnr;
	}
	@Override
	public String toString() {
		return vejnavn+" "+husnr+", "+postnummer+" "+postnavn;
	}
}
