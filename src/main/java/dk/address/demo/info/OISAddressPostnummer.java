package dk.address.demo.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Postnummer, child of Address entity from OIS service: http://geo.oiorest.dk/adresser.json?q=hulg+67
 * 
 * @author Tau
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OISAddressPostnummer {
	String nr;
	String navn;
	
	public String getNr() {
		return nr;
	}
	public void setNr(String nr) {
		this.nr = nr;
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
}
