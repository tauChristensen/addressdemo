package dk.address.demo.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Address (root) entity from OIS service: http://geo.oiorest.dk/adresser.json?q=hulg+67 
 * 
 * @author Tau
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OISAddress {

    private String husnr;
    private OISAddressVejnavn vejnavn;
    private OISAddressPostnummer postnummer;
    private OISAddressKommune kommune;
    
	public String getHusnr() {
		return husnr;
	}
	public void setHusnr(String husnr) {
		this.husnr = husnr;
	}
	public OISAddressVejnavn getVejnavn() {
		return vejnavn;
	}
	public void setVejnavn(OISAddressVejnavn vejnavn) {
		this.vejnavn = vejnavn;
	}
	public OISAddressPostnummer getPostnummer() {
		return postnummer;
	}
	public void setPostnummer(OISAddressPostnummer postnummer) {
		this.postnummer = postnummer;
	}
	public OISAddressKommune getKommune() {
		return kommune;
	}
	public void setKommune(OISAddressKommune kommune) {
		this.kommune = kommune;
	}
}
