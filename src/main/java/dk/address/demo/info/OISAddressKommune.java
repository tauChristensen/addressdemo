package dk.address.demo.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Kommune, child of Address entity from OIS service: http://geo.oiorest.dk/adresser.json?q=hulg+67
 * 
 * @author Tau
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OISAddressKommune {
	
	private String kode;
	private String navn;
	
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
}
