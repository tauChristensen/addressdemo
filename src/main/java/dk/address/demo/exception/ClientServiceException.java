package dk.address.demo.exception;

/**
 * Exception form underlying data layer.
 * 
 * @author Tau
 *
 */
public class ClientServiceException extends Exception {

	/**
	 * Serialize exception to AddressDemo application clients
	 */
	private static final long serialVersionUID = 1657395363982356564L;

	public ClientServiceException() {
		super();
	}

	public ClientServiceException(String message) {
		super(message);
	}

	public ClientServiceException(Throwable exception) {
		super(exception);
	}

	public ClientServiceException(String message, Throwable exception) {
		super(message, exception);
	}

	public ClientServiceException(String message, Throwable cause, 
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
