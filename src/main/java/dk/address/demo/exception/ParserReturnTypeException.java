package dk.address.demo.exception;

public class ParserReturnTypeException extends Exception {
	/**
	 * Serialize exception to AddressDemo application clients
	 */
	private static final long serialVersionUID = 1657395363982356565L;

	public ParserReturnTypeException() {
		super();
	}

	public ParserReturnTypeException(String message) {
		super(message);
	}

	public ParserReturnTypeException(Throwable exception) {
		super(exception);
	}

	public ParserReturnTypeException(String message, Throwable exception) {
		super(message, exception);
	}

	public ParserReturnTypeException(String message, Throwable cause, 
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
