package dk.address.demo.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dk.address.demo.dao.ClientServiceDaoInterface;
import dk.address.demo.dao.OISAddressRESTDao;
import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.KVHAddress;
import dk.address.demo.utility.Address2KVHAddress;

@RestController
public class RESTKVHService {
	static Logger logger = Logger.getLogger(RESTKVHService.class);
			
	ClientServiceDaoInterface<KVHAddress> service = new OISAddressRESTDao<KVHAddress>();
	
	@RequestMapping("/kvh")
    public List<KVHAddress> getKVH(@RequestParam(value="address", defaultValue="") String address) throws ClientServiceException, ParserReturnTypeException {
		logger.info("SpringRestService.getKVH: "+((address!=null)?address:"null"));

		//TODO: Return Optional and pack exceptions

		return service.getData(new Address2KVHAddress(),address);
    }
}
