package dk.address.demo;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	static Logger logger = Logger.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		logger.info("Application started");
		SpringApplication.run(DemoApplication.class, args);
	}

}
