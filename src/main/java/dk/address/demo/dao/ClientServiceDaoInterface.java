package dk.address.demo.dao;

import java.util.List;

import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.utility.Response2ReturnType;

public interface ClientServiceDaoInterface<T> {
	
	/**
	 * Get data from underlying model.
	 * 
	 * @param parser Function that produce instances of T from response objects.
	 * @param request Request parameters
	 * @return list of return data from underlying model 
	 * @throws ClientServiceException Exception from communicating with underlying model
	 * @throws ParserReturnTypeException  Exception from parsing return data into instances of T
	 */
	public List<T> getData(Response2ReturnType<T> parser, Object... request) throws ClientServiceException, ParserReturnTypeException;
	
	/**
	 * Parse instance of response data into List of T. 
	 * 
	 * @param response List of requested object entities from underlying data storage.
	 * @param parser Function that produce instances of T from response objects.
	 * @return list of T objects from response.
	 * @throws ParserReturnTypeException Thrown if response cannot be passed to list of T.
	 */
	public List<T> parseResponse(Object[] response, Response2ReturnType<T> parser) throws ParserReturnTypeException;
}
