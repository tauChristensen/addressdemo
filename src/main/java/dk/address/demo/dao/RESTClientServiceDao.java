/**
 * 
 */
package dk.address.demo.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.utility.Response2ReturnType;

/**
 * @author Tau
 * @param <T>
 *
 */
public abstract class RESTClientServiceDao<T> implements ClientServiceDaoInterface<T> {
	static Logger logger = Logger.getLogger(RESTClientServiceDao.class);

	@Override
	public abstract List<T> getData(Response2ReturnType<T> parser, Object... request) throws ClientServiceException, ParserReturnTypeException;

	/**
	 * Parse list of Addresses into List of T. 
	 * 
	 * @param items Response from REST service marshaled into list of Address.
	 * @param parser Function that produce T from Address instance.
	 * @return list of T addresses from oisRespons, containing only the first rows (10) addresses.
	 * @throws ParserReturnTypeException Thrown if data can not be passed to list of T.
	 */
	public List<T> parseResponse(Object[] items, Response2ReturnType<T> parser) throws ParserReturnTypeException {
		List<T> response = new ArrayList<T>(); 
		Arrays.stream(items).forEach(item -> {
			try {
				response.add(parser.parse(item));
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}	
		});
		
		if (items.length != response.size())
			throw new ParserReturnTypeException("Cannot parse data into return type.");
		
		logger.info("Got " + response.size() + " items...");
		
		return response;
	}
}
