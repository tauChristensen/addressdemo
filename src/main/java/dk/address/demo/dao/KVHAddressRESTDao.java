package dk.address.demo.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import dk.address.demo.exception.ClientServiceException;
import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.KVHAddress;
import dk.address.demo.utility.Response2ReturnType;

/**
 * Test Service to request addresses from AddressDemo application.
 * 
 * @author Tau
 *
 * @param <T>
 */

public class KVHAddressRESTDao<T> extends RESTClientServiceDao<T> {
	static Logger logger = Logger.getLogger(KVHAddressRESTDao.class);
	
	private final String url = "http://localhost:8080/kvh";
	
	/**
	 * Get data from this application.
	 * 
	 * @param request Request parameters. For this service "q" containing a string matching address.
	 * @param parser Function to transform Address to KVHAddress.
	 * @return List of KVH addresses
	 * @throws RestClientException Exception from Spring service
	 * @throws ParserReturnTypeException Exception from parsing data into instances of T
	 */
	public List<T> getData(Response2ReturnType<T> parser, Object... request) throws ClientServiceException, ParserReturnTypeException {
		try {
			logger.info("KVHAddressRESTDao.getData: "+(request!=null?(String)request[0]:"null"));
			RestTemplate restTemplate = new RestTemplate();
			//TODO: Spring did not send query parameter "address" as expected when send as Map, "address" added to the URL.
			return parseResponse(restTemplate.getForObject(url+"?address="+(String)request[0], KVHAddress[].class), parser);
		}
		catch(RestClientException e) {
			throw new ClientServiceException(e.getMessage(), e);
		}
	}	
}
