package dk.address.demo.utility;

import dk.address.demo.exception.ParserReturnTypeException;

/**
 * Parser interface for converting data into given type.
 * 
 * @author Tau
 *
 * @param <T> Parse Address to T
 */

public interface Response2ReturnType<T> {
	
	/**
	 * Parse input object to return type object.
	 * Parser must be 
	 * @param item object that will be passed to return type.
	 * @return instance of return type, with data from item.
	 * @throws ParserReturnTypeException Exception is thrown if there is no rules for converting item into return type.
	 */
	public T parse(Object item) throws ParserReturnTypeException;
}
