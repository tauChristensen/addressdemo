package dk.address.demo.utility;

import org.apache.log4j.Logger;

import dk.address.demo.exception.ParserReturnTypeException;
import dk.address.demo.info.KVHAddress;
import dk.address.demo.info.OISAddress;

/**
 * Convert IOS response Address to KVH Address response.
 * 
 * @author Tau
 *
 */
public class Address2KVHAddress implements Response2ReturnType<KVHAddress> {
	static Logger logger = Logger.getLogger(Address2KVHAddress.class);
	
	public KVHAddress parse(Object item) throws ParserReturnTypeException {
		if (!(item==null || item instanceof OISAddress))
			throw new ParserReturnTypeException("Item must be instance of Address.");
			
		OISAddress address = (OISAddress)item;
		KVHAddress kvhaddress = null;
		
		if (address != null
			&& address.getKommune() != null
			&& address.getPostnummer() != null
			&& address.getVejnavn() != null) {
				
			kvhaddress = new KVHAddress();
			
			kvhaddress.setKommune(address.getKommune().getKode());
			kvhaddress.setVejnummer(address.getVejnavn().getKode());
			kvhaddress.setHusnr(address.getHusnr());
			kvhaddress.setPostnummer(address.getPostnummer().getNr());
			kvhaddress.setPostnavn(address.getPostnummer().getNavn());
			kvhaddress.setVejnavn(address.getVejnavn().getNavn());
			
			logger.info("Getting address: "+kvhaddress.toString());
		}
		
		return kvhaddress;
	}

}
